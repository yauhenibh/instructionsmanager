﻿using System.Threading.Tasks;

namespace Core.BLL.Abstractions.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
