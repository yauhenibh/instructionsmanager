﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Core.BLL.DTO.InstructionDTO;
using Core.DAL.Entities.Domain;

namespace Core.BLL.Abstractions.Services
{
    public interface IInstructionService
    {
        List<InstructionSummaryDTO> GetAllInstructions();

        Task<List<InstructionSummaryDTO>> GetAllInstructionByAuthorIdAsync(int authorId);

        Task<InstructionContentDTO> GetInstructionToViewById(int id);

        Task<InstructionDTO> GetByIdAndAuthorIdAsync(int id, int authorId);

        Task<List<InstructionPreviewDTO>> GetAllPreviewAsync();

        Task<int> AddOrUpdateAsync(Instruction instruction);

        Task<int> DeleteAsync(int id);
    }
}
