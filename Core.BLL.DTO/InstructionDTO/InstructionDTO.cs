﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.BLL.DTO.InstructionDTO
{
    public class InstructionDTO
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Summary { get; set; }

        public string Content { get; set; }
    }
}
