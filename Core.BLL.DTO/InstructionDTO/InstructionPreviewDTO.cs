﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.BLL.DTO.InstructionDTO
{
    public class InstructionPreviewDTO
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Summary { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime? LastUpdate { get; set; }

        public string AuthorUserName { get; set; }

        public string AuthorAboutMe { get; set; }

    }
}
