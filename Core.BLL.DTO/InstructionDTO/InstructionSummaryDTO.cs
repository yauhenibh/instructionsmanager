﻿using System;

namespace Core.BLL.DTO.InstructionDTO
{
    public class InstructionSummaryDTO
    {
        public int Id { get; set; }
        
        public string Title { get; set; }

        public string Summary { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime? LastUpdate { get; set; }
    }
}
