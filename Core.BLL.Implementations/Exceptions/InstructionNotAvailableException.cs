﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.BLL.Implementations.Exceptions
{
    public class InstructionNotAvailableException: Exception
    {
        public InstructionNotAvailableException() : base("Instruction does not exist or is not available")
        {
            
        }
    }
}
