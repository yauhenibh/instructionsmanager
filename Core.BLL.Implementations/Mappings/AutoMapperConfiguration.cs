﻿using AutoMapper;
using WebUI.Mapping;

namespace Core.BLL.Implementations.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<DomainToDTOMappingProfile>();
                x.AddProfile<DTOToDomainMappingProfile>();
            });
        }
    }
}