﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Core.BLL.DTO.InstructionDTO;
using Core.DAL.Entities.Domain;

namespace WebUI.Mapping
{
    public class DTOToDomainMappingProfile : Profile
    {
        public DTOToDomainMappingProfile()
        {
            CreateMap<InstructionDTO, Instruction>();
        }
    }
}