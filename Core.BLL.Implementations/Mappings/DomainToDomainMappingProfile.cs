﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Core.DAL.Entities.Domain;

namespace Core.BLL.Implementations.Mappings
{
    public class DomainToDomainMappingProfile: Profile
    {
        public DomainToDomainMappingProfile()
        {
            CreateMap<Instruction, Instruction>()
                .ForMember(s => s.CreatedTime, m => m.Ignore())
                .ForMember(s => s.LastUpdate, m => m.Ignore());
        }
    }
}
