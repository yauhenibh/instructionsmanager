﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Core.BLL.Abstractions.Services;
using Core.BLL.DTO.InstructionDTO;
using Core.BLL.Implementations.Exceptions;
using Core.Data;
using Core.Data.Repositories;
using Core.DAL.Entities.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions.Internal;

namespace Core.BLL.Implementations.Services
{
    public class InstructionService : IInstructionService
    {
        private readonly IGenericRepository<Instruction> _instructionRepository;
        private readonly IUnitOfWork _unitOfWork;
        public InstructionService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _instructionRepository = unitOfWork.Repository<Instruction>();
        }
        public List<InstructionSummaryDTO> GetAllInstructions()
        {
            var events = _instructionRepository.Collection.ToList();
            return Mapper.Map<List<Instruction>, List<InstructionSummaryDTO>>(events);
        }

        public async Task<List<InstructionSummaryDTO>> GetAllInstructionByAuthorIdAsync(int authorId)
        {
           return await _instructionRepository.Collection.Where(s => s.AuthorId == authorId)
                .Select(x => new InstructionSummaryDTO
                {
                    Id = x.Id,
                    Title = x.Title,
                    CreatedTime = x.CreatedTime,
                    LastUpdate = x.LastUpdate,
                    Summary = x.Summary
                }).ToListAsync();
        }

        public async Task<InstructionContentDTO> GetInstructionToViewById(int id)
        {
            return await _instructionRepository.Collection.Where(s => s.Id == id)
                .Select(s => new InstructionContentDTO
                {
                    Id = s.Id,
                    Summary = s.Summary,
                    Title = s.Title,
                    CreatedTime = s.CreatedTime,
                    LastUpdate = s.LastUpdate,
                    Content = s.Content,
                    AuthorUserName = s.Author.UserName,
                    AuthourAboutMe = s.Author.AboutAuthor,
                    AuthourFacebook = s.Author.FacebookId,
                    AuthourInstagram = s.Author.AboutAuthor
                }).FirstOrDefaultAsync();
        }

        public async Task<InstructionDTO> GetByIdAndAuthorIdAsync(int id, int authorId)
        {
            var instruction = await _instructionRepository.Collection.Where(s=>s.Id==id&&s.AuthorId==authorId).Select(s=> new InstructionDTO
            {
                Id = s.Id,
                Summary = s.Summary,
                Title = s.Title,
                Content = s.Content
            }).FirstOrDefaultAsync();
            if (instruction == null)
            {
                throw  new InstructionNotAvailableException();
            }
            return instruction;
        }

        public async Task<List<InstructionPreviewDTO>> GetAllPreviewAsync()
        {
            return await _instructionRepository.Collection.Select(s => new InstructionPreviewDTO
                {
                    Id = s.Id,
                    Summary = s.Summary,
                    CreatedTime = s.CreatedTime,
                    LastUpdate = s.LastUpdate,
                    Title = s.Title,
                    AuthorUserName = s.Author.UserName,
                    AuthorAboutMe = s.Author.AboutAuthor
                })
                .ToListAsync();
        }

        public async Task<int> AddOrUpdateAsync(Instruction instruction)
        {
            if (instruction == null)
            {
                throw new ArgumentNullException(nameof(instruction));
            }
            var instructionInDb = await _instructionRepository.CollectionWithTracking.FirstOrDefaultAsync(s => s.Id == instruction.Id);
            if (instructionInDb == null)
            {
                instruction.CreatedTime = DateTime.UtcNow;
                _instructionRepository.Insert(instruction);
            }
            else
            {
                instructionInDb.LastUpdate = DateTime.UtcNow;
                Mapper.Map(instruction, instructionInDb);
                _instructionRepository.Update(instructionInDb);
            }
            return await _unitOfWork.SaveChangesAsync();
        }

        public async Task<int> DeleteAsync(int id)
        {
            var instruction = _instructionRepository.Collection.FirstOrDefault(s => s.Id == id);
            if (instruction == null)
            {
                throw new InstructionNotAvailableException();
            }
            _instructionRepository.Delete(instruction);
            return await _unitOfWork.SaveChangesAsync();
        }
    }
}
