﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Core.DAL.Entities.Contracts;

namespace Core.Data.Repositories
{
    public interface IGenericRepository<TEntity> where TEntity : class, IEntity
    { 
        IQueryable<TEntity> Collection { get; }

        IQueryable<TEntity> CollectionWithTracking { get; }

        void Insert(params TEntity[] items);

        void Delete(params TEntity[] items);

        void Update(params TEntity[] items);
    }
}
