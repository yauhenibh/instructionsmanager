﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.DAL.Entities.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Core.DAL.Data.Configuration
{
 
    public class InstructionConfiguration : IEntityTypeConfiguration<Instruction>
    {
        public void Configure(EntityTypeBuilder<Instruction> builder)
        {
            builder.ToTable("Instructions");

            builder.Property(p => p.Id)
                .HasColumnName("Id");
        }
    }
}
