﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Core.DAL.Data.Factories
{
    public class BloggingContextFactory : IDesignTimeDbContextFactory<Context.ApplicationContext>
    {
        public Context.ApplicationContext CreateDbContext(string[] args)
        {
            if (args == null || !args.Any())
            {

                throw new ValidationException("Provide connections string value");
            }
            var optionsBuilder = new DbContextOptionsBuilder<Context.ApplicationContext>();
            optionsBuilder.UseSqlServer($"@{args.First()}");

            return new Context.ApplicationContext(optionsBuilder.Options);

        }
    }
}
