﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.DAL.Entities.Contracts
{
    public interface IEntity
    {
       int Id { get; set; }
    }
}
