﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.DAL.Entities.Contracts;
using Core.DAL.Entities.Identity;

namespace Core.DAL.Entities.Domain
{
    public class Instruction: IEntity, ICreateUpdateTracker
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public string Summary { get; set; }

        public User Author { get; set; }

        public int AuthorId { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime? LastUpdate { get; set; }
    }
}
