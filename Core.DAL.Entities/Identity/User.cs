﻿using Core.DAL.Entities.Contracts;
using Microsoft.AspNetCore.Identity;

namespace Core.DAL.Entities.Identity
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class User : IdentityUser<int>, IEntity
    {
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public string FacebookId { get; set; }

        public string InstagramId { get; set; }

        public string AboutAuthor { get; set; }

    }
}
