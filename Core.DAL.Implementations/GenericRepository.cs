﻿using System;
using System.Linq;
using Core.Data.Repositories;
using Core.DAL.Data.Context;
using Core.DAL.Entities.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Core.DAL.Implementations
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity>
        where TEntity : class, IEntity
    {
        protected readonly ApplicationContext _context;
        protected readonly DbSet<TEntity> _dbSet;

        public GenericRepository(ApplicationContext context)
        {
            _context = context;
            _dbSet = _context.Set<TEntity>();
        }
        public IQueryable<TEntity> Collection => _dbSet.AsNoTracking();

        public IQueryable<TEntity> CollectionWithTracking => _dbSet;

        public void Insert(params TEntity[] items)
        {
            if (items == null)
            {
                throw new NullReferenceException("There are no items to insert");
            }

            foreach (var item in items)
            {
                _dbSet.Add(item);
            }
        }

        public virtual void Delete(params TEntity[] items)
        {
            if (items == null)
            {
                throw new NullReferenceException("There are no items to delete");
            }

            foreach (var item in items)
            {
                if (_context.Entry(item).State == EntityState.Detached)
                {
                    _dbSet.Attach(item);
                }
                _dbSet.Remove(item);
            }
        }

        public void Update(params TEntity[] items)
        {
            if (items == null)
            {
                throw new NullReferenceException("There are no items to update");
            }
            foreach (var item in items)
            {

                _dbSet.Update(item);
            }
        }


    }
}
