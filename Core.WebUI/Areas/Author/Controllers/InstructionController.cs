﻿using System.Threading.Tasks;
using AutoMapper;
using Core.BLL.Abstractions.Services;
using Core.BLL.DTO.InstructionDTO;
using Core.DAL.Entities.Domain;
using Core.WebUI.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Core.WebUI.Areas.Author.Controllers
{
    [Authorize(Roles = "Author")]
    [Area("Author")]
    public class InstructionController: Controller
    {
        private readonly IInstructionService _instructionService;

        public InstructionController(IInstructionService instructionService)
        {
            _instructionService = instructionService;
        }

        public async Task<IActionResult> Index()
        {
        
            return View(await _instructionService.GetAllInstructionByAuthorIdAsync(this.User.GetUserId()));
        }
        [HttpGet]
        public ViewResult Create()
        {
            ViewData["Action"] = "Create";
            return View("AddEditInstuction", new InstructionDTO());
        }

        [HttpGet]
        public async Task<ViewResult> Update(int id)
        {
            var instruction = await _instructionService.GetByIdAndAuthorIdAsync(id, this.User.GetUserId());
            ViewData["Action"] = "Edit";
            return View("AddEditInstuction", instruction);
        }

        [HttpPost]
        public async Task<IActionResult> AddOrUpdate(InstructionDTO model)
        {
            var instruction = Mapper.Map<InstructionDTO, Instruction>(model);
            instruction.AuthorId = this.User.GetUserId();
            await _instructionService.AddOrUpdateAsync(instruction);
            return RedirectToAction("Index");
        }

        
        public async Task<IActionResult> Delete(int id)
        {            
            await _instructionService.DeleteAsync(id);
            return RedirectToAction("Index");
        }
    }
}
