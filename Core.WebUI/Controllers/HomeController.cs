﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Core.BLL.Abstractions.Services;
using Core.BLL.DTO.InstructionDTO;
using Core.DAL.Entities.Domain;
using Microsoft.AspNetCore.Mvc;
using Core.WebUI.Models;

namespace Core.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly IInstructionService _instructionService;

        public HomeController(IInstructionService instructionService)
        {
            _instructionService = instructionService;
        }

        public async Task<IActionResult> Index()
        {
           
            return View(await _instructionService.GetAllPreviewAsync());
        }

        public async Task<IActionResult> Read(int id)
        {          
            return View(await _instructionService.GetInstructionToViewById(id));
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
