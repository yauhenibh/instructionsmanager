﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.BLL.Abstractions.Services;
using Microsoft.AspNetCore.Mvc;

namespace Core.WebUI.Controllers
{
    public class InstructionsController : Controller
    {

        private readonly IInstructionService _instructionService;

        public InstructionsController(IInstructionService instructionService)
        {
            _instructionService = instructionService;
        }
        public IActionResult Index()
        {
            return View(_instructionService.GetAllInstructions());
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View(_instructionService.GetAllInstructions());
        }


        [HttpPost]
        public IActionResult AddOrUpdate()
        {
            return View(_instructionService.GetAllInstructions());
        }
    }
}