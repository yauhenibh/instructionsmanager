﻿using System;
using System.Security.Claims;

namespace Core.WebUI.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static int GetUserId(this ClaimsPrincipal principal)
        {
            if (principal == null)
                throw new ArgumentNullException(nameof(principal));

            return Int32.Parse(principal.FindFirst(ClaimTypes.NameIdentifier)?.Value);
        }
    }
}
