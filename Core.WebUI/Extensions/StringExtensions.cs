﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.WebUI.Extensions
{
    public static class StringExtensions
    {
        public static string Truncate(this string s, int max,bool withDots = true)
        {
            string dots = withDots ? "..." : string.Empty;
            return s?.Length > max ? $"{s.Substring(0, max)}{dots}" : s ?? throw new ArgumentNullException(s);
        }
    }
}
