﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.DAL.Data.Context;
using Core.DAL.Entities.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Core.WebUI.Identity
{
    public class ApplicationRoleStore: RoleStore<Role, ApplicationContext, int, UserRole, RoleClaim>
    {
        public ApplicationRoleStore(ApplicationContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }
    }
}
