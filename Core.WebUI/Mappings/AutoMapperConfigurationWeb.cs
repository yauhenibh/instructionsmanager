﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Core.BLL.Implementations.Mappings;
using Core.WebUI.Mappings;
using WebUI.Mapping;

namespace Core.WebUI.Mappings
{
    public class AutoMapperConfigurationWeb
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<DTOToViewModelMappingProfile>();
                x.AddProfile<ViewModelToDTOMappingProfile>();
                x.AddProfile<DTOToDomainMappingProfile>();
                x.AddProfile<DomainToDTOMappingProfile>();
                x.AddProfile<DomainToDomainMappingProfile>();
            });
        }
    }
}