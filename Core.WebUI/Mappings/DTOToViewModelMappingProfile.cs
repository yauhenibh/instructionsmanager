﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Core.BLL.DTO.InstructionDTO;
using Core.DAL.Entities.Domain;
using Core.DAL.Entities.Identity;
using Core.WebUI.Models.ManageViewModels;

namespace Core.WebUI.Mappings
{
    public class DTOToViewModelMappingProfile : Profile
    {
        public DTOToViewModelMappingProfile()
        {
            CreateMap<Instruction, InstructionSummaryDTO>();

            CreateMap<User, ProfileViewModel>();
        }
    }
}