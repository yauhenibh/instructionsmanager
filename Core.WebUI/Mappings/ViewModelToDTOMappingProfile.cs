﻿using AutoMapper;
using Core.DAL.Entities.Identity;
using Core.WebUI.Models.AccountViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.WebUI.Models.ManageViewModels;

namespace Core.WebUI.Mappings
{
    public class ViewModelToDTOMappingProfile: Profile
    {
        public ViewModelToDTOMappingProfile()
        {
            CreateMap<RegisterViewModel, User>();

            CreateMap<ProfileViewModel, User>()
                .ForMember(s=>s.UserName, m=>m.Ignore())
                .ForMember(s=>s.Email, m=>m.Ignore());
        }
    }
}
