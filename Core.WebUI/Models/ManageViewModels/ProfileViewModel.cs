﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core.WebUI.Models.ManageViewModels
{
    public class ProfileViewModel
    {
        [Display(Name="User Name")]
        public string Username { get; set; }

        public string Email { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Facebook Link")]
        public string FacebookId { get; set; }

        [Display(Name = "Instagram Link")]
        public string InstagramId { get; set; }

        [Display(Name = "About Author")]
        public string AboutAuthor { get; set; }
    }
}
