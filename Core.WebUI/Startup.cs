﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.BLL.Abstractions.Services;
using Core.BLL.Implementations.Mappings;
using Core.BLL.Implementations.Services;
using Core.Data;
using Core.DAL.Data.Context;
using Core.DAL.Entities.Identity;
using Core.WebUI.Identity;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Core.WebUI.Models;
using Data;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using WebUI.Mapping;
using Core.WebUI.Mappings;

namespace Core.WebUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;


 
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            var connectionString = Configuration.GetConnectionString(Environment.IsDevelopment() ? "LocalConnection" : "ProductionConnection");
            services.AddDbContext<ApplicationContext>(options =>
                options.UseSqlServer(connectionString));
            services
                .AddScoped<UserStore<User, Role, ApplicationContext, int, UserClaim, UserRole, UserLogin, UserToken,
                    RoleClaim>, ApplicationUserStore>();
            services
                .AddScoped<RoleStore<Role, ApplicationContext, int, UserRole, RoleClaim>, ApplicationRoleStore>();

            services.AddIdentity<User, Role>()

                .AddUserStore<ApplicationUserStore>()
                .AddRoleStore<ApplicationRoleStore>()
                .AddDefaultTokenProviders();

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<IInstructionService, InstructionService>();

            services.AddMvc();
            //services.AddExtCore();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            if (Environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                InitializeDatabase(app);
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "areas",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            //app.UseExtCore();
            ConfigureMappers();
        }


        public void ConfigureMappers()
        {
            AutoMapperConfigurationWeb.Configure();
        }

        private void InitializeDatabase(IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                scope.ServiceProvider.GetRequiredService<ApplicationContext>().Database.Migrate();
                if(!scope.ServiceProvider.GetRequiredService<ApplicationContext>().Roles.Any(s=>s.Name=="Author")) { 
                scope.ServiceProvider.GetRequiredService<ApplicationContext>()
                    .Roles.Add(new Role
                    {
                        Name = "Author",
                        NormalizedName = "AUTHOR"
                    });
                scope.ServiceProvider.GetRequiredService<ApplicationContext>().SaveChanges();
                    }
            }
        }
    }
}
